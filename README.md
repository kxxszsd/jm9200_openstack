# jm9200_openstack

#### 介绍
本仓库是openstack的一键部署脚本。本仓库代码只适配了麒麟V10 sp2系统。

#### 软件架构
基于OpenStack的GPU直通解决方案


#### 安装教程

详见《JM9200-OpenStack直通虚拟化方案部署手册》

#### 使用说明

详见《JM9200-OpenStack直通虚拟化方案部署手册》

